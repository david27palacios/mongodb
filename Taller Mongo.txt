use mongo1;

db.createCollection("Profesores");

db.Profesores.insertMany([
	{
	  "nombre":"Oscar",
	  "apellido":"Palacios",
	  "edad":"35",
	  "asignatura":"Espa�ol",
	  "correo":"oscar@gmail.com",
	  "documento":"1000858118",
	},{
	  "nombre":"Felipe",
	  "apellido":"Gonzalez",
	  "edad":"42",
	  "asignatura":"Informatica",
	  "correo":"pipegonz@gmail.com",
	  "documento":"145259946",
	},{
	  "nombre":"Beatriz",
	  "apellido":"Patilla",
	  "edad":"62",
	  "asignatura":"Religion",
	  "correo":"soyhp@gmail.com",
	  "documento":"488104118",
	},{
	  "nombre":"Sandra",
	  "apellido":"Huertos",
	  "edad":"44",
	  "asignatura":"Ingles",
	  "correo":"sandrita@gmail.com",
	  "documento":"481124118",
	},{
	  "nombre":"Jhon ",
	  "apellido":"Forero",
	  "edad":"33",
	  "asignatura":"Seguimiento Proyecto",
	  "correo":"jhonf45@gmail.com",
	  "documento":"85411",
	}
	])

db.Profesores.find().pretty();

db.createCollection("Estudiantes");

db.Estudiantes.insertMany([
	{
	"nombre":"Juan",
	"apellido":"Alima�a",
	"edad":"13",
	"correo":"juana@gmail.com",
	"documento":"14528943",
	},{
	"nombre":"Benito",
	"apellido":"Martinez",
	"edad":"12",
	"correo":"benitomart654@gmail.com",
	"documento":"546528943",
	},{
	"nombre":"Esteban",
	"apellido":"Jimenez",
	"edad":"14",
	"correo":"ESTB14@gmail.com",
	"documento":"31254821",
	},{
	"nombre":"Cristian",
	"apellido":"Zuluaga",
	"edad":"17",
	"correo":"CR7458@gmail.com",
	"documento":"145896",
	},{
	"nombre":"Jhonatan",
	"apellido":"Lopez",
	"edad":"18",
	"correo":"JhonatanSoloMillos@gmail.com",
	"documento":"7895421",
	},{
	"nombre":"Harold",
	"apellido":"Mu�oz",
	"edad":"15",
	"correo":"Harolitalsw@gmail.com",
	"documento":"4564831",
	},{
	"nombre":"Sallary",
	"apellido":"Nailen",
	"edad":"15",
	"correo":"Sallas15w@gmail.com",
	"documento":"454896489",
	},{
	"nombre":"Milena",
	"apellido":"Aguilera",
	"edad":"12",
	"correo":"Milenitakitty@gmail.com",
	"documento":"36898974",
	},{
	"nombre":"Alma Marcela",
	"apellido":"Gosa",
	"edad":"17",
	"correo":"dealegria@gmail.com",
	"documento":"458476",
	},{
	"nombre":"Dilan",
	"apellido":"Alima�a",
	"edad":"19",
	"correo":"Dilan10@gmail.com",
	"documento":"98785451",
	},{
	"nombre":"Alejandra",
	"apellido":"Sanchez",
	"edad":"17",
	"correo":"Alejandra17@gmail.com",
	"documento":"4815665616",
	},{
	"nombre":"Sech",
	"apellido":"Luzci",
	"edad":"17",
	"correo":"Esteessech@gmail.com",
	"documento":"156482",
	},{
	"nombre":"Carlos",
	"apellido":"Olarte",
	"edad":"13",
	"correo":"Carlitos@gmail.com",
	"documento":"988528943",
	},{
	"nombre":"Javier",
	"apellido":"Balvin",
	"edad":"15",
	"correo":"JBalvin@gmail.com",
	"documento":"6587943",
	},{
	"nombre":"Juan",
	"apellido":"Perez",
	"edad":"18",
	"correo":"JP48s@gmail.com",
	"documento":"487422323",
	},{
	"nombre":"Jesus",
	"apellido":"Ropero",
	"edad":"19",
	"correo":"jrropero14@gmail.com",
	"documento":"14528943",
	},{
	"nombre":"Adriana",
	"apellido":"Camelas",
	"edad":"15",
	"correo":"Camelas2@gmail.com",
	"documento":"5006853",
	},{
	"nombre":"Alejandro",
	"apellido":"Hernandez",
	"edad":"13",
	"correo":"AlejitoHZ2@gmail.com",
	"documento":"698754200",
	},{
	"nombre":"Luis",
	"apellido":"Pereira",
	"edad":"17",
	"correo":"solomedallo14@gmail.com",
	"documento":"9984845",
	},{
	"nombre":"Camila",
	"apellido":"Melo",
	"edad":"16",
	"correo":"camels47@gmail.com",
	"documento":"4575656",
	}
])

db.Estudiantes.find().pretty();

db.createCollection("Cursos");

db.Cursos.insertMany([
	{
	"nombre_curso":"1101",
	"cantidad_creditos":"15",
	},{
	"nombre_curso":"1103",
	"cantidad_creditos":"15",
	},{
	"nombre_curso":"1102",
	"cantidad_creditos":"15",
	},{
	"nombre_curso":"701",
	"cantidad_creditos":"10",
	},{
	"nombre_curso":"702",
	"cantidad_creditos":"15",
	},{
	"nombre_curso":"703",
	"cantidad_creditos":"15",
	},{
	"nombre_curso":"601",
	"cantidad_creditos":"8",
	},{
	"nombre_curso":"801",
	"cantidad_creditos":"11",
	},
])
db.Cursos.find().pretty();
db.createCollection("profesoresCurso")
db.profesoresCurso.insertMany([
	{
	"profesor":new ObjectId("5fad65b3c340055d4a62d49f"),
	"curso":new ObjectId("5fad6f758d7e44182928621a"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d49f"),
	"curso":new ObjectId("5fad6f758d7e44182928621b"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a0"),
	"curso":new ObjectId("5fad6f758d7e44182928621a"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a1"),
	"curso":new ObjectId("5fad6f758d7e44182928621d"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a2"),
	"curso":new ObjectId("5fad6f758d7e44182928621e"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a2"),
	"curso":new ObjectId("5fad6f758d7e44182928621f"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a3"),
	"curso":new ObjectId("5fad6f758d7e441829286220"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a0"),
	"curso":new ObjectId("5fad65b3c340055d4a62d4a3"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d49f"),
	"curso":new ObjectId("5fad6f758d7e441829286220"),
	},{
	"profesor":new ObjectId("5fad65b3c340055d4a62d4a0"),
	"curso":new ObjectId("5fad6f758d7e441829286221"),
	},
	
])
db.profesoresCurso.find().pretty();
db.createCollection("estudiantesCurso")
db.estudiantesCurso.insertMany([

	{
	"estudiante":new ObjectId("5fad6d748d7e441829286206"),
	"curso":new ObjectId("5fad6f758d7e44182928621a"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286207"),
	"curso":new ObjectId("5fad6f758d7e44182928621a"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286208"),
	"curso":new ObjectId("5fad6f758d7e44182928621b"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286209"),
	"curso":new ObjectId("5fad6f758d7e44182928621b"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620a"),
	"curso":new ObjectId("5fad6f758d7e44182928621b"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620b"),
	"curso":new ObjectId("5fad6f758d7e44182928621c"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620c"),
	"curso":new ObjectId("5fad6f758d7e44182928621c"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620d"),
	"curso":new ObjectId("5fad6f758d7e44182928621d"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620e"),
	"curso":new ObjectId("5fad6f758d7e44182928621d"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e44182928620f"),
	"curso":new ObjectId("5fad6f758d7e44182928621e"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286210"),
	"curso":new ObjectId("5fad6f758d7e44182928621e"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286211"),
	"curso":new ObjectId("5fad6f758d7e44182928621f"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286212"),
	"curso":new ObjectId("5fad6f758d7e44182928621f"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286213"),
	"curso":new ObjectId("5fad6f758d7e441829286220"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286214"),
	"curso":new ObjectId("5fad6f758d7e441829286220"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286215"),
	"curso":new ObjectId("5fad6f758d7e441829286221"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286216"),
	"curso":new ObjectId("5fad6f758d7e441829286221"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286217"),
	"curso":new ObjectId("5fad6f758d7e44182928621d"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286218"),
	"curso":new ObjectId("5fad6f758d7e44182928621c"),
	},{
	"estudiante":new ObjectId("5fad6d748d7e441829286219"),
	"curso":new ObjectId("5fad6f758d7e44182928621a"),
	},
])

db.Profesores.aggregate([
{
 $match:{
	 _id : ObjectId("5fad65b3c340055d4a62d49f"),
		  },		 
	},
	{
	  $lookup:{
		     from:'profesoresCurso',
		     localField:'_id',
		     foreignField:'profesor',
		     as:'cursosProfesor'
		  }
	},
	{
	 	$unwind:'$cursosProfesor'
	},
	{
		$project:{
			   nombreProfesor:'$nombre',
			   apellidoProfesor:'$apellido',
			   correoProfesor:'$correo"',
			   edad:'$edad',
			   documento:'$documento',
			   Profesor:'$cursosProfesor.profesor',
			   Cursos:'$cursosProfesor.curso'
			 }
	},
	
]).pretty();




db.Cursos.aggregate([

	{
	  $match:{
		     _id : ObjectId("5fad6f758d7e441829286220"),
		 },
	},
	{
	  $lookup:{
		     from:'estudiantesCurso',
		     localField:'_id',
		     foreignField:'curso',
		     as:'estudiantesCurso'
		  }
	},{
	 	$unwind:'$estudiantesCurso'
	},
	{
		$project:{
			   NombreCurso:'$nombre',
			   CreditosCurso:'$creditos',
			   Estudiante:'$estudiantesCurso.estudiante',
			   Curso:'$estudiantesCurso.curso'
			 }
	},
	
]).pretty()
